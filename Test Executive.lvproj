﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Custom Type Test Executive" Type="Folder">
			<Item Name="Custom Type Test Executive.lvlib" Type="Library" URL="../Source/Custom Type Test Executive/Custom Type Test Executive.lvlib"/>
		</Item>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@cs" Type="Folder">
				<Item Name="data-type-serializer" Type="Folder">
					<Item Name="source" Type="Folder">
						<Item Name="Assertion" Type="Folder">
							<Item Name="Assertion.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Assertion/Assertion.lvclass"/>
						</Item>
						<Item Name="Assertion 1D" Type="Folder">
							<Item Name="Assertion 1D.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Assertion 1D/Assertion 1D.lvclass"/>
						</Item>
						<Item Name="Boolean" Type="Folder">
							<Item Name="Boolean.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Boolean/Boolean.lvclass"/>
						</Item>
						<Item Name="Boolean 1D" Type="Folder">
							<Item Name="Boolean 1D.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Boolean 1D/Boolean 1D.lvclass"/>
						</Item>
						<Item Name="Data Type" Type="Folder">
							<Item Name="Data Type.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Data Type/Data Type.lvclass"/>
						</Item>
						<Item Name="Double" Type="Folder">
							<Item Name="Double.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Double/Double.lvclass"/>
						</Item>
						<Item Name="Double 1D" Type="Folder">
							<Item Name="Double 1D.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Double 1D/Double 1D.lvclass"/>
						</Item>
						<Item Name="I32" Type="Folder">
							<Item Name="I32.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/I32/I32.lvclass"/>
						</Item>
						<Item Name="I32 1D" Type="Folder">
							<Item Name="I32 1D.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/I32 1D/I32 1D.lvclass"/>
						</Item>
						<Item Name="Path" Type="Folder">
							<Item Name="Path.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Path/Path.lvclass"/>
						</Item>
						<Item Name="Path 1D" Type="Folder">
							<Item Name="Path 1D.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Path 1D/Path 1D.lvclass"/>
						</Item>
						<Item Name="String" Type="Folder">
							<Item Name="String.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/String/String.lvclass"/>
						</Item>
						<Item Name="String 1D" Type="Folder">
							<Item Name="String 1D.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/String 1D/String 1D.lvclass"/>
						</Item>
						<Item Name="Timestamp" Type="Folder">
							<Item Name="Timestamp.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/Timestamp/Timestamp.lvclass"/>
						</Item>
						<Item Name="U32" Type="Folder">
							<Item Name="U32.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/U32/U32.lvclass"/>
						</Item>
						<Item Name="U32 1D" Type="Folder">
							<Item Name="U32 1D.lvclass" Type="LVClass" URL="../gpm_packages/@cs/data-type-serializer/source/U32 1D/U32 1D.lvclass"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/data-type-serializer/CHANGELOG.md"/>
					<Item Name="Data Types.lvproj" Type="Document" URL="../gpm_packages/@cs/data-type-serializer/Data Types.lvproj"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/data-type-serializer/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/data-type-serializer/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/data-type-serializer/README.md"/>
				</Item>
				<Item Name="linked-list" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Linked List" Type="Folder">
							<Item Name="Linked Lists.lvlib" Type="Library" URL="../gpm_packages/@cs/linked-list/Source/Linked List/Linked Lists.lvlib"/>
						</Item>
						<Item Name="Sandbox" Type="Folder">
							<Item Name="Basic Linked List Test.vi" Type="VI" URL="../gpm_packages/@cs/linked-list/Source/Sandbox/Basic Linked List Test.vi"/>
							<Item Name="Persist Test.vi" Type="VI" URL="../gpm_packages/@cs/linked-list/Source/Sandbox/Persist Test.vi"/>
							<Item Name="Pointer Test.vi" Type="VI" URL="../gpm_packages/@cs/linked-list/Source/Sandbox/Pointer Test.vi"/>
							<Item Name="Replace Test.vi" Type="VI" URL="../gpm_packages/@cs/linked-list/Source/Sandbox/Replace Test.vi"/>
							<Item Name="Serialization Test.vi" Type="VI" URL="../gpm_packages/@cs/linked-list/Source/Sandbox/Serialization Test.vi"/>
						</Item>
						<Item Name="String Manipulation" Type="Folder">
							<Item Name="String Manipulation.lvlib" Type="Library" URL="../gpm_packages/@cs/linked-list/Source/String Manipulation/String Manipulation.lvlib"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/linked-list/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/linked-list/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/linked-list/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/linked-list/README.md"/>
				</Item>
				<Item Name="lookup-table" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="LookupTable" Type="Folder">
							<Item Name="LookupTable.lvlib" Type="Library" URL="../gpm_packages/@cs/lookup-table/Source/LookupTable/LookupTable.lvlib"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/lookup-table/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/lookup-table/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/lookup-table/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/lookup-table/README.md"/>
				</Item>
				<Item Name="persistence" Type="Folder">
					<Item Name="source" Type="Folder">
						<Item Name="CAML" Type="Folder">
							<Item Name="CAML.lvlib" Type="Library" URL="../gpm_packages/@cs/persistence/source/CAML/CAML.lvlib"/>
						</Item>
						<Item Name="IPersist" Type="Folder">
							<Item Name="IPersist.lvlib" Type="Library" URL="../gpm_packages/@cs/persistence/source/IPersist/IPersist.lvlib"/>
						</Item>
						<Item Name="XML" Type="Folder">
							<Item Name="XML.lvlib" Type="Library" URL="../gpm_packages/@cs/persistence/source/XML/XML.lvlib"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/persistence/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/persistence/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/persistence/LICENSE"/>
					<Item Name="Persistence.lvproj" Type="Document" URL="../gpm_packages/@cs/persistence/Persistence.lvproj"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/persistence/README.md"/>
				</Item>
				<Item Name="run-time-assertions" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="Run Time Assertions.lvlib" Type="Library" URL="../gpm_packages/@cs/run-time-assertions/Source/Run Time Assertions.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/run-time-assertions/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/run-time-assertions/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/run-time-assertions/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/run-time-assertions/README.md"/>
				</Item>
				<Item Name="variant" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="VariantExtensions.lvlib" Type="Library" URL="../gpm_packages/@cs/variant/Source/VariantExtensions.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/variant/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/variant/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/variant/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/variant/README.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Integration Testing" Type="Folder">
			<Item Name="Sandbox" Type="Folder">
				<Item Name="Case Structure Test.vi" Type="VI" URL="../Source/Sandbox/Case Structure Test.vi"/>
				<Item Name="Copy Test.vi" Type="VI" URL="../Source/Sandbox/Copy Test.vi"/>
				<Item Name="Duplicate Test.vi" Type="VI" URL="../Source/Sandbox/Duplicate Test.vi"/>
				<Item Name="Mock Executioner.lvclass" Type="LVClass" URL="../Source/Sandbox/While Loop Stop Example/Mock Executioner/Mock Executioner.lvclass"/>
				<Item Name="Mock Test Step.lvclass" Type="LVClass" URL="../Source/Sandbox/While Loop Stop Example/Mock Test Step/Mock Test Step.lvclass"/>
				<Item Name="Orphaned Pointer Test.vi" Type="VI" URL="../Source/Sandbox/Orphaned Pointer Test.vi"/>
				<Item Name="Relink Test.vi" Type="VI" URL="../Source/Sandbox/Relink Test.vi"/>
				<Item Name="Simple Test Executive Test.vi" Type="VI" URL="../Source/Sandbox/Simple Test Executive Test.vi"/>
				<Item Name="Subroutine Example.vi" Type="VI" URL="../Source/Sandbox/Subroutine Example/Subroutine Example.vi"/>
				<Item Name="Test Restore Point.vi" Type="VI" URL="../Source/Sandbox/Test Restore Point.vi"/>
				<Item Name="While Loop Stop Example.vi" Type="VI" URL="../Source/Sandbox/While Loop Stop Example/While Loop Stop Example.vi"/>
			</Item>
			<Item Name="Nested Conditional in While Loop Example.lvlib" Type="Library" URL="../Source/Sandbox/Nested Conditional in While Loop Example/Nested Conditional in While Loop Example.lvlib"/>
			<Item Name="Parallelizer Example.lvlib" Type="Library" URL="../Source/Sandbox/Parallelizer Example/Parallelizer Example.lvlib"/>
		</Item>
		<Item Name="Loggin Interface" Type="Folder">
			<Item Name="ILog.lvlib" Type="Library" URL="../Source/Logging/ILog/ILog.lvlib"/>
			<Item Name="Simple Log.lvlib" Type="Library" URL="../Source/Logging/Simple Log/Simple Log.lvlib"/>
		</Item>
		<Item Name="Test Executive.lvlib" Type="Library" URL="../Source/Test Executive/Test Executive.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Is Value Changed.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Is Value Changed.vim"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MD5Checksum core.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum core.vi"/>
				<Item Name="MD5Checksum File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum File.vi"/>
				<Item Name="MD5Checksum format message-digest.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum format message-digest.vi"/>
				<Item Name="MD5Checksum pad.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/MD5Checksum.llb/MD5Checksum pad.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
